// import redis from 'redis'
import colors from 'colors'
import Logger from '../helpers/Logger'
import { createClient } from 'then-redis'


const client = createClient({
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
    password: process.env.REDIS_PASSWORD,
});

client.on('error', (err) => {
    Logger.error("Ha ocurrido un error", err)
})
client.on('ready', () => {
    Logger.info(`${colors.magenta('[  DB  ]')} *** Conectada`)
})

export default client