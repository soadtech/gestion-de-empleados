export const country = {
    colombia: 1,
    usa: 2
}
export const domains = {
    colombia: 'cidenet.com.co',
    usa: 'cidenet.com.us'
}
export const countries = [
    {
        id: country.colombia,
        name: 'Colombia'
    },
    {
        id: country.usa,
        name: 'Estados Unidos'
    },
]

export const typeDocuments = [
    {
        id: 1,
        name: 'Cédula de Ciudadanía'
    },
    {
        id: 2,
        name: 'Cédula de Extranjería'
    },
    {
        id: 3,
        name: 'Pasaporte'
    },
    {
        id: 4,
        name: 'Permiso Especial'
    }
]

export const areas = [
    {
        id: 1,
        name: 'Administración'
    },
    {
        id: 2,
        name: 'Financiera'
    },
    {
        id: 3,
        name: 'Compras'
    },
    {
        id: 4,
        name: 'Infraestructura'
    },
    {
        id: 5,
        name: 'Operación'
    },
    {
        id: 6,
        name: 'Talento Humano'
    },
    {
        id: 7,
        name: 'Servicios Varios'
    }
]

export const allData = {
    country: countries,
    typeDocument: typeDocuments,
    area: areas
}
