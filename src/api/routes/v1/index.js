import { Router } from 'express'
import employeesRoutes from './employees'
const router = Router();

export default function () {
    router.use("/employees", employeesRoutes());
    return router;
};
