import { Router } from 'express'
import * as employees from './access'
import schema from './access/schema';
import validator from '../../../middleware/validator';
import validatorArraysDefaults from '../../../middleware/validatorArraysDefaults';
import { countries } from '../../../../helpers/constants';

const router = Router();

export default function () {
    router.post("/", validator(schema.create), validatorArraysDefaults(schema.create), employees.create);
    router.delete("/:document", employees.remove);
    router.get("/", employees.get);
    router.get("/filter", employees.filter);
    return router;
};