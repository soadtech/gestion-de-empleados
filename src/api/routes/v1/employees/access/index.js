import { SuccessResponse, InternalError } from "../../../../../helpers//api.response"
import EmployeeService from "../../../../../services/employees.service";
import AccessService from "../../../../../services/access.service";

const employeeService = EmployeeService.getInstance();
const accessService = AccessService.getInstance();

export const create = async (req, res) => {
    const { surname, secondSurname, firstName, otherName, country, typeDocument, document, area, status } = req.body;

    try {
        const result = await employeeService.getByDocument(document)
        if (result.key.length > 0) return SuccessResponse(res, false, 'Employee already exists')

        const { key } = await accessService.create(document)
        const email = await employeeService.generateEmail(firstName, surname, country)
        const { data } = await employeeService.create(key, document, typeDocument, email, country, area, status, surname, secondSurname, firstName, otherName)
        return SuccessResponse(res, true, 'Employee added successfully', { data: { data } })
    } catch (error) {
        return InternalError(res)
    }
}

export const remove = async (req, res) => {
    const { document } = req.params
    try {
        await employeeService.removeByDocument(document)
        return SuccessResponse(res, true, 'Employee remove successfully')
    } catch (error) {
        return InternalError(res)
    }
}

export const get = async (_, res) => {
    try {
        const data = await employeeService.getAll()
        const employees = []
        if (data.length > 0) {
            let cont = 0

            while (cont < data.length) {
                const employee = data[cont]
                const exits = []
                let cont2 = 0;
                while (cont2 < employee.access.length) {
                    const result = await accessService.getByKey(employee.access[cont2])
                    exits.push(result)
                    cont2++
                }
                employees.push({ ...employee, access: exits })
                cont++
            }
        }

        return SuccessResponse(res, true, 'Success', { data: { employees } })
    } catch (error) {
        return InternalError(res)
    }
}

export const filter = async (req, res) => {
    const { text } = req.query
    try {
        const data = await employeeService.getAll()
        const filters = []
        if (data.length > 0) {
            let cont = 0
            const objEmployee = data[0]
            const employees = []
            while (cont < data.length) {
                const employee = data[cont]
                const exits = []
                let cont2 = 0;

                while (cont2 < employee.access.length) {
                    const result = await accessService.getByKey(employee.access[cont2])
                    exits.push(result)
                    cont2++
                }
                employees.push({ ...employee, access: exits })
                cont++
            }

            let cont3 = 0;
            const objKeys = Object.keys(objEmployee)
            while (cont3 < objKeys.length) {
                let cont4 = 0;
                let expresion = new RegExp(`${text.toUpperCase()}.*`, "i");
                while (cont4 < employees.length) {
                    const keyValue = objKeys[cont3]
                    if (keyValue == 'country' || keyValue == 'typeDocument') {
                        if (expresion.test(employees[cont4][keyValue].name)) {
                            filters.push(employees[cont4])
                        }
                    } else {
                        if (expresion.test(employees[cont4][keyValue])) {
                            filters.push(employees[cont4])
                        }
                    }

                    cont4++
                }
                cont3++
            }
        }



        return SuccessResponse(res, true, 'Success', { data: { filters } })
    } catch (error) {
        return InternalError(res)
    }
}