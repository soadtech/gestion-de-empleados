export default {
    create: {
        surname: {
            type: 'string',
            require: true
        },
        secondSurname: {
            type: 'string',
            require: true
        },
        firstName: {
            type: 'string',
            require: true
        },
        otherName: {
            type: 'string',
            require: false
        },
        country: {
            type: 'string',
            require: true
        },
        typeDocument: {
            type: 'string',
            require: true
        },
        document: {
            type: 'string',
            require: true
        },
        area: {
            type: 'string',
            require: true
        },
        status: {
            type: 'number',
            require: true
        },
        dateAccess: {
            type: 'string',
            require: true
        }
    },
    delete: {
        document: {
            type: 'number',
            require: true
        }
    }
}