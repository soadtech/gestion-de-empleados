export default (body, schema) => {
    const regexEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
    const keys = Object.keys(schema)
    let error = ''
    let cont = 0;
    let find = false;
    while (cont < keys.length && !find) {
        const value = body[keys[cont]];
        const valueSchema = schema[keys[cont]]
        // Validando que exista el parametro
        if (!value && valueSchema.require) {
            error = `${keys[cont]} is required`
            find = true
        }

        // Validando que sea del mismo tipo
        if (typeof value !== valueSchema.type && value) {
            error = `${keys[cont]} It is not a ${valueSchema.type} type`
            find = true
        }

        // Validando email
        if (value && !regexEmail.test(value) && valueSchema.email) {
            error = `${keys[cont]} is not valid`
            find = true
        }
        cont++
    }
    return error
}