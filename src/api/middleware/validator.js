import { BadRequestError } from "../../helpers/api.response"
import findError from "./findError";

export default (schema) => (req, res, next) => {
    const body = req.body
    const error = findError(body, schema);

    if (error == '') return next()

    return BadRequestError(res, error)
}