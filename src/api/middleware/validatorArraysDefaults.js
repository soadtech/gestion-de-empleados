import { BadRequestError } from "../../helpers/api.response"
import { allData } from "../../helpers/constants";

export default (schema) => (req, res, next) => {
    const keys = Object.keys(schema)
    const body = req.body
    let find = false, cont = 0;
    let error = ''
    while (cont < keys.length && !find) {
        const value = body[keys[cont]];
        const arraySchema = allData[keys[cont]]
        if (arraySchema) {
            let countFind = 0
            arraySchema.forEach(e => {
                if (e.id == value) {
                    countFind++
                }
            })
            if (countFind == 0) {
                find = true;
                error = `${value} Is not a valid ${keys[cont]}`
            }
        }
        cont++
    }

    if (error.length === 0) return next()

    return BadRequestError(res, error)
}