import colors from 'colors'
import Logger from '../helpers/logger'
import AccessModel from '../models/access.model'
import { genUuid } from '../helpers/uuid'

const accessModel = AccessModel.getInstance()
export default class AccessService {
    static instance

    static getInstance () {
        if (!AccessService.instance) {
            AccessService.instance = new AccessService()
        }
        return AccessService.instance
    }

    create = async (idEmployee) => {
        try {
            const uuid = genUuid()
            const today = new Date()
            const data = {
                idEmployee,
                uuid,
                date: today
            }
            const result = await accessModel.create(uuid, idEmployee, today, data)
            return { ...result, data }
        } catch (error) {
            Logger.error(colors.red('Error AccessService create '), error)
            throw new Error('ERROR TECNICO')
        }
    }

    getByKey = async (key) => {
        try {
            const result = await accessModel.getByKey(key)
            return result
        } catch (error) {
            Logger.error(colors.red('Error AccessService getByKey '), error)
            throw new Error('ERROR TECNICO')
        }
    }
}