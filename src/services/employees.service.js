import colors from 'colors'
import Logger from '../helpers/logger'
import EmployeeModel from '../models/employee.model'
import { areas, countries, typeDocuments, country, domains } from '../helpers/constants'
import getNameOperation from '../helpers/getNameOperation'

const employeeModel = EmployeeModel.getInstance()
export default class EmployeeService {
    static instance

    static getInstance () {
        if (!EmployeeService.instance) {
            EmployeeService.instance = new EmployeeService()
        }
        return EmployeeService.instance
    }

    create = async (idAccess, document, typeDocument, email, country, area, status, surname, secondSurname, firstName, otherName) => {
        try {
            const data = {
                document,
                typeDocument: getNameOperation(typeDocument, typeDocuments),
                email,
                country: getNameOperation(country, countries),
                area: getNameOperation(area, areas),
                status,
                surname,
                secondSurname,
                firstName,
                otherName,
                access: [idAccess],
                exist: []
            }
            const result = await employeeModel.create(document, typeDocument, email, country, area, status, data)
            return { ...result, data }
        } catch (error) {
            Logger.error(colors.red('Error EmployeeService create '), error)
            throw new Error('ERROR TECNICO')
        }
    }

    getByDocument = async (document) => {
        try {
            const result = await employeeModel.getByDocument(document)
            return result
        } catch (error) {
            Logger.error(colors.red('Error EmployeeService getByDocument '), error)
            throw new Error('ERROR TECNICO')
        }
    }

    generateEmail = async (firstName, surname, countr) => {
        try {
            const cleanSurname = surname.replace(/ /g, '');
            const cleanFirstName = firstName.replace(/ /g, '')
            let domain = countr == country.colombia ? domains.colombia : domains.usa
            let email = `${cleanFirstName}.${cleanSurname}@${domain}`
            const { key } = await employeeModel.getByEmail(email)
            if (key.length > 0) {
                const lastId = await employeeModel.getLastId(domain)
                email = `${cleanFirstName}.${cleanSurname}.${lastId.data}@${domain}`
            }
            return email
        } catch (error) {
            Logger.error(colors.red('Error EmployeeService generateEmail '), error)
            throw new Error('ERROR TECNICO')
        }
    }

    removeByDocument = async (document) => {
        try {
            const { key } = await employeeModel.getByDocument(document)
            if (key.length > 0) {
                await employeeModel.removeByDocument(key[0])
            }
            return 'OK'
        } catch (error) {
            Logger.error(colors.red('Error EmployeeService removeByDocument '), error)
            throw new Error('ERROR TECNICO')
        }
    }

    getAll = async () => {
        try {
            const key = await employeeModel.getAll()
            const data = []
            if (key.length > 0) {
                let cont = 0
                while (cont < key.length) {
                    const dataUser = await employeeModel.getByKey(key[cont]);
                    data.push(JSON.parse(dataUser))
                    cont++
                }
            }

            return data
        } catch (error) {
            Logger.error(colors.red('Error EmployeeService getAll '), error)
            throw new Error('ERROR TECNICO')
        }
    }
}