import colors from 'colors'
import Logger from '../helpers/logger'
import ExitModel from '../models/exits.model'
import { genUuid } from '../helpers/uuid'

const exitModel = ExitModel.getInstance()
export default class ExitService {
    static instance

    static getInstance () {
        if (!ExitService.instance) {
            ExitService.instance = new ExitService()
        }
        return ExitService.instance
    }

    create = async (idEmployee) => {
        try {
            const uuid = genUuid()
            const today = new Date()
            const data = {
                idEmployee,
                uuid,
                date: today
            }
            const result = await exitModel.create(uuid, idEmployee, today, data)
            return result
        } catch (error) {
            Logger.error(colors.red('Error ExitService create '), error)
            throw new Error('ERROR TECNICO')
        }
    }
}