import client from '../loaders/redis'

export default class ExitModel {
    static instance

    static getInstance () {
        if (!ExitModel.instance) {
            ExitModel.instance = new ExitModel()
        }
        return ExitModel.instance
    }

    create = async (uuid, idEmployee, date, data) => {
        const key = `exit_${uuid}_${idEmployee}_${date}`
        const result = await client.set(key, JSON.stringify(data))

        return { key, result }
    }
}