import client from '../loaders/redis'

export default class AccessModel {
    static instance

    static getInstance () {
        if (!AccessModel.instance) {
            AccessModel.instance = new AccessModel()
        }
        return AccessModel.instance
    }

    create = async (uuid, idEmployee, date, data) => {
        const key = `access_${uuid}_${idEmployee}_${date}`
        const result = await client.set(key, JSON.stringify(data))

        return { key, result }
    }

    getByKey = async (key) => {
        const result = await client.get(key)
        return result == null ? result : JSON.parse(result)
    }
}