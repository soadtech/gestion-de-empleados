import client from '../loaders/redis'

export default class EmployeeModel {
    static instance

    static getInstance () {
        if (!EmployeeModel.instance) {
            EmployeeModel.instance = new EmployeeModel()
        }
        return EmployeeModel.instance
    }

    create = async (document, typeDocument, email, country, area, status, data) => {
        const result = await client.set(`employee_${document}_${typeDocument}_${email}_${country}_${area}_${status}_1`, JSON.stringify(data))

        return result
    }
    getByDocument = async (document) => {
        const key = await client.keys(`employee_${document}_*_*_*_*_*_1`)
        let data = {}
        if (key.length > 0) {
            const dataUser = await client.get(key[0])
            data = JSON.parse(dataUser)
        }
        return { key, data }
    }
    getByEmail = async (email) => {
        const key = await client.keys(`employee_*_*_${email}_*_*_*_1*`)
        let data = {}
        if (key.length > 0) {
            const dataUser = await client.get(key[0])
            data = JSON.parse(dataUser)
        }
        return { key, data }
    }
    getLastId = async (domain) => {
        const key = await client.keys(`lastIdEmail_${domain}`)
        let data = 1
        if (key.length > 0) {
            data = await client.get(key[0])
        }
        await client.set(`lastIdEmail_${domain}`, JSON.stringify(parseInt(data) + 1))
        return { key, data }
    }
    removeByDocument = async (key) => {
        const keySplit = key.split("_");
        keySplit[7] = 0
        const newKey = keySplit.join('_')
        const result = await client.rename(key, newKey)
        return result
    }
    getAll = async () => {
        const result = await client.keys('employee_*_*_*_*_*_*_1')
        return result
    }
    getByKey = async (key) => {
        const result = await client.get(key)
        return result
    }
}