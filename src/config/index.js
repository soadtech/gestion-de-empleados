require("dotenv").config();

export const port = process.env.PORT
export const dev = process.env.ENVIROMENT
export const name = "API"

export const api = {
    prefix: '/api/v1'
}
