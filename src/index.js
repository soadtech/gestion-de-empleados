import express from "express";
import Logger from "./helpers/Logger";
import { name, port } from "./config";
import colors from "colors";

async function startServer () {
    const app = express();
    await require("./loaders").default(app);

    app
        .listen(port, () => {
            Logger.info(`${colors.yellow(
                "########################################################"
            )}
  🛡️        ${colors.green(
                `Server ${colors.blue(name)} listening on port:`
            )} ${colors.blue(port)} 🛡️
${colors.yellow(
                "########################################################"
            )}`);
        })
        .on("error", (e) => Logger.error("error in server.listen", e));

}

startServer()
    .then(() => Logger.info(colors.green("Done ✌️")))
    .catch((error) =>
        Logger.error(colors.red("Error when starting the api"), error)
    );